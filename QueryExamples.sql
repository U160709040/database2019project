   #1
SELECT avg(Years_coding)
FROM Developer_Career join Personal_Info on Developer_Career.idPersonal_Info =  Personal_Info.idPersonal_Info
WHERE Country = “USA”;

 #2
SELECT LanguageWorked_With
FROM Developer_Choices join Personal_Info on Personal_Info.idPersonal_Info = Developer_Choices.idPersonal_Info
WHERE Age like "18-24%";

#3
SELECT count(id_Developer)
FROM Developer_Preferences
WHERE Operating_System like "% Linux %" and id_Developer in
(SELECT id_Developer
FROM Developer_Choices
WHERE Opern_Source = “Yes”);

#4
SELECT avg(Salary)
FROM Salary join Developer_Career on Salary.idPersonal_Info = Developer_Career.idPersonal_Info
WHERE Dev_Type = "Database Administrator";


#5 
SELECT count(IDE) as Count_IDE, IDE
FROM Developer_Preferences join Developer_Career on Developer_Preferences.id_Developer = Developer_Career.id_Developer 
WHERE Years_Coding like "0-2%"
group by IDE
order by Count_IDE desc
limit 3;

#6
SELECT count(id_Developer)
FROM Developer_Preferences join Personal_Info on Developer_Preferences.idPersonal_Info = Personal_Info.idPersonal_Info
WHERE Version_Control like "%Merucial%" and(Country = “Turkey” or Country = “Russia”);

#7
SELECT count(FrameWorked_DesireNextYear) as Count_Frame,FrameWorked_DesireNextYear
FROM Developer_Choices join Personal_Info on Developer_Choices.idPersonal_Info = Personal_Info.idPersonal_Info
WHERE Gender = "Female"
group by FrameWorked_DesireNextYear
order by Count_Frame desc
limit 5;


#8
 SELECT count(idEducation)
FROM Education
WHERE FormalEducation like "Master’s degree%" and idAI in
(SELECT idAI
FROM AI
WHERE AI_Future like "I’m worried about%");
                

#9
 SELECT count(idEducation)
FROM Education join Developer_Career on Education.idPersonal_Info = Developer_Career.idPersonal_Info
WHERE UndergradMajor not like "%Computer Science%" and Years_Coding != “0-2”;


#10
SELECT count(id_Developer)
FROM Stackoverflow_Usage join Student on Stackoverflow_Usage.idPersonal_Info = Student.idPersonal_Info
WHERE Stackoverflow_HasAccount like  "Yes%" and Student like  "Yes%" and idPersonal_Info in
(SELECT Age
FROM idPersonal_Info
WHERE Age like "25 - 34%");

#SELECT *
#FROM Personal_Info
#WHERE Age like "25 - 34%";
